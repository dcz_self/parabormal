pub mod graph;
pub mod pipeline;

use bevy::ecs::component::Component;

#[derive(Component)]
pub struct Target;

#[derive(Component)]
pub struct Camera;
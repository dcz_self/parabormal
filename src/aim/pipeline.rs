/* SPDX-License-Identifier: MIT or Apache-2.0
Based on Bevy
commit ea4aeff9ec8e204b8d4bc8cbb10c28e90fe271ff
file crates/bevy_pbr/src/wireframe.rs
*/
/*! A plugin for rendering the hit distribution of a projectile following a parabolic path.
*/

use bevy::pbr::MeshPipeline;
use bevy::pbr::{DrawMesh, MeshPipelineKey, MeshUniform, SetMeshBindGroup, SetMeshViewBindGroup};
use bevy::asset::{AssetServer, Handle};
use bevy::core_pipeline::core_3d::Opaque3d;
use bevy::ecs::prelude::*;
use bevy::render::{
    mesh::{Mesh, MeshVertexBufferLayout},
    render_asset::RenderAssets,
    render_phase::{AddRenderCommand, DrawFunctions, RenderPhase, SetItemPipeline},
    render_resource::{
        PipelineCache, RenderPipelineDescriptor, Shader, SpecializedMeshPipeline,
        SpecializedMeshPipelineError, SpecializedMeshPipelines,
    },
    view::{ExtractedView, Msaa, VisibleEntities},
    RenderApp, RenderStage,
};
use bevy::render::extract_component::UniformComponentPlugin;
use bevy::utils::tracing::error;

use std::borrow::Cow;
use super::graph::AimPhase;

#[derive(Debug, Default)]
pub struct Plugin;

impl bevy::app::Plugin for Plugin {
    fn build(&self, app: &mut bevy::app::App) {
        // not sure, would this produce dynamic index? what is that even?
        app.add_plugin(UniformComponentPlugin::<TargetUniform>::default());
    
        if let Ok(render_app) = app.get_sub_app_mut(RenderApp) {
            render_app
                .add_render_command::<AimPhase, Draw>()
                .init_resource::<Pipeline>()
                .init_resource::<SpecializedMeshPipelines<Pipeline>>()
                .add_system_to_stage(RenderStage::Extract, extract_meshes)
                .add_system_to_stage(RenderStage::Queue, queue_meshes)
                .add_system_to_stage(RenderStage::Queue, queue_target_bind_group);
        }
    }
}

use bevy::render::Extract;
use bevy::render::render_resource::BindGroupLayout;
use bevy::render::render_resource::BindGroupLayoutDescriptor;
use bevy::render::render_resource::{BindGroupLayoutEntry, BindingType, BufferBindingType, ShaderStages};
use bevy::render::render_resource::ShaderType;
use bevy::render::renderer::RenderDevice;
use bevy::render::view::ComputedVisibility;
use crate::aim::Target;


#[derive(Resource)]
pub struct Pipeline {
    mesh_pipeline: MeshPipeline,
    shader: Handle<Shader>,
    target_layout: BindGroupLayout,
}

#[derive(Component, ShaderType, Clone)]
pub struct TargetUniform {
    pub flags: u32,
}

fn extract_meshes(
    mut commands: Commands,
    mut prev_caster_commands_len: Local<usize>,
    meshes_query: Extract<
        Query<(
            Entity,
            &ComputedVisibility,
            &Handle<Mesh>,
            Option<With<Target>>,
        )>,
    >,
) {
    let mut caster_commands = Vec::with_capacity(*prev_caster_commands_len);
    let visible_meshes = meshes_query.iter().filter(|(_, vis, ..)| vis.is_visible());

    for (entity, _, handle, target) in visible_meshes {
        let flags = if target.is_some() {
            1
        } else {
            0
        };
        let uniform = TargetUniform { flags };
        caster_commands.push((entity, (handle.clone_weak(), uniform)));
    }
    *prev_caster_commands_len = caster_commands.len();
    commands.insert_or_spawn_batch(caster_commands);
}

impl FromWorld for Pipeline {
    fn from_world(render_world: &mut World) -> Self {
        let shader = render_world
            .resource::<AssetServer>()
            .load("shaders/aim.wgsl");
        Self {
            mesh_pipeline: render_world.resource::<MeshPipeline>().clone(),
            shader,
            target_layout: render_world
                .resource::<RenderDevice>()
                .create_bind_group_layout(&BindGroupLayoutDescriptor {
                    label: Some("target_layout"),
                    entries: &[BindGroupLayoutEntry {
                        binding: 0,
                        visibility: ShaderStages::VERTEX | ShaderStages::FRAGMENT,
                        ty: BindingType::Buffer {
                            ty: BufferBindingType::Uniform,
                            has_dynamic_offset: true,
                            min_binding_size: Some(TargetUniform::min_size()),
                        },
                        count: None,
                    }],
                }),
        }
    }
}

impl SpecializedMeshPipeline for Pipeline {
    type Key = MeshPipelineKey;

    fn specialize(
        &self,
        key: Self::Key,
        layout: &MeshVertexBufferLayout,
    ) -> Result<RenderPipelineDescriptor, SpecializedMeshPipelineError> {
        let mut descriptor = self.mesh_pipeline.specialize(key, layout)?;
        descriptor.vertex.shader = self.shader.clone_weak();
        // maybe here for accepting the target.output_texture() format:
        // descriptor.fragment.as_mut().unwrap.format = FOO;
        descriptor.fragment.as_mut().unwrap().shader = self.shader.clone_weak();
        descriptor.depth_stencil.as_mut().unwrap().bias.slope_scale = 1.0;
        descriptor.label = Some(Cow::from("aim_pipeline"));
        if let Some(ref mut l) = &mut descriptor.layout {
            l.push(self.target_layout.clone());
        }
        Ok(descriptor)
    }
}

#[allow(clippy::too_many_arguments)]
fn queue_meshes(
    opaque_3d_draw_functions: Res<DrawFunctions<AimPhase>>,
    render_meshes: Res<RenderAssets<Mesh>>,
    wireframe_pipeline: Res<Pipeline>,
    mut pipelines: ResMut<SpecializedMeshPipelines<Pipeline>>,
    mut pipeline_cache: ResMut<PipelineCache>,
    msaa: Res<Msaa>,
    material_meshes: Query<
        (Entity, &Handle<Mesh>, &MeshUniform),
    >,
    mut views: Query<(&ExtractedView, &VisibleEntities, &mut RenderPhase<AimPhase>)>,
) {
    let draw_custom = opaque_3d_draw_functions
        .read()
        .get_id::<Draw>()
        .unwrap();
    let msaa_key = MeshPipelineKey::from_msaa_samples(msaa.samples);
    for (view, visible_entities, mut opaque_phase) in &mut views {
        let rangefinder = view.rangefinder3d();

        let view_key = msaa_key | MeshPipelineKey::from_hdr(view.hdr);
        let add_render_phase =
            |(entity, mesh_handle, mesh_uniform): (Entity, &Handle<Mesh>, &MeshUniform)| {
                if let Some(mesh) = render_meshes.get(mesh_handle) {
                    let key = view_key
                        | MeshPipelineKey::from_primitive_topology(mesh.primitive_topology);
                    let pipeline_id = pipelines.specialize(
                        &mut pipeline_cache,
                        &wireframe_pipeline,
                        key,
                        &mesh.layout,
                    );
                    let pipeline_id = match pipeline_id {
                        Ok(id) => id,
                        Err(err) => {
                            error!("{}", err);
                            return;
                        }
                    };
                    opaque_phase.add(AimPhase::new(Opaque3d {
                        entity,
                        pipeline: pipeline_id,
                        draw_function: draw_custom,
                        distance: rangefinder.distance(&mesh_uniform.transform),
                    }));
                }
            };

        let query = &material_meshes;
        visible_entities
            .entities
            .iter()
            .filter_map(|visible_entity| query.get(*visible_entity).ok())
            .for_each(add_render_phase);
    }
}

type Draw = (
// TODO: give the shader info about projectile motion:
// velocity, gravity. Probably the camera should carry this info best.
    SetItemPipeline,
    SetMeshViewBindGroup<0>,
    SetMeshBindGroup<1>,
    SetTargetBindGroup<2>,
    DrawMesh,
);


use bevy::ecs::system::SystemParamItem;
use bevy::ecs::system::lifetimeless::Read;
use bevy::ecs::system::lifetimeless::{SQuery, SRes};
use bevy::render::extract_component::ComponentUniforms;
use bevy::render::extract_component::DynamicUniformIndex;
use bevy::render::render_phase::EntityRenderCommand;
use bevy::render::render_phase::RenderCommandResult;
use bevy::render::render_phase::TrackedRenderPass;
use bevy::render::render_resource::BindGroup;
use bevy::render::render_resource::BindGroupDescriptor;
use bevy::render::render_resource::BindGroupEntry;

// a bind group is just a place for data it seems.
// it may be a resource or a component.
// for meshes it's a resource, and each material gets one.
// For us, there's one material containing a bool: target or not.

// For projectile info, th bind group should be a resource on the camera.

// where does this buffer get filled?
// in extract_meshes
// we'll have to hang on and insert the uniforms to existing entities.
// afterwards ComponentUniforms should kick in maybe

#[derive(Resource)]
struct TargetBindGroup(BindGroup);

fn queue_target_bind_group(
    mut commands: Commands,
    aim_pipeline: Res<Pipeline>,
    render_device: Res<RenderDevice>,
    target_uniforms: Res<ComponentUniforms<TargetUniform>>,
) {
    if let Some(binding) = target_uniforms.uniforms().binding() {
        let bind_group = TargetBindGroup(
            render_device.create_bind_group(&BindGroupDescriptor {
                entries: &[BindGroupEntry {
                    binding: 0,
                    resource: binding.clone(),
                }],
                label: Some("target_bind_group"),
                layout: &aim_pipeline.target_layout,
            })
        );
        commands.insert_resource(bind_group);
    }
}

// TODO: attach more camera info
struct SetTargetBindGroup<const I: usize>;

impl<const I: usize> EntityRenderCommand for SetTargetBindGroup<I> {
    type Param = (
        SRes<TargetBindGroup>,
        SQuery<Read<DynamicUniformIndex<TargetUniform>>>,
    );
    
    #[inline]
    fn render<'w>(
        _view: Entity,
        item: Entity,
        (bind_group, query): SystemParamItem<'w, '_, Self::Param>,
        pass: &mut TrackedRenderPass<'w>,
    ) -> RenderCommandResult {
        let target = query.get(item).unwrap();
        pass.set_bind_group(I, &bind_group.into_inner().0, &[target.index()]);
        RenderCommandResult::Success
    }
}

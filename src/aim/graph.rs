/*! Establishes the render graph for rendering the projectile shooting camera.

This will:
- render visible parts of the target in uniform color
- find the center of mass of the visible shape
- render visible parts of the target with normal distribution respective to the shot center
- sum up the visible parts and return the value as % hit chance
- save the depth texture (for illumination from another direction)
*/

use bevy::app::App;

use bevy::core_pipeline::core_3d::Camera3d;
// FIXME: get rid of this
use bevy::core_pipeline::upscaling::UpscalingNode;

use bevy::ecs::system::{Commands, Query};
use bevy::ecs::entity::Entity;
use bevy::ecs::query::With;

use bevy::render::{Extract, RenderApp, RenderStage};
use bevy::render::camera::Camera;
use bevy::render::render_graph::{RenderGraph, SlotInfo, SlotType};
use bevy::render::render_phase::{DrawFunctions, RenderPhase};
use bevy::render::render_phase::sort_phase_system;
use bevy::render::view::ViewTarget;

use crate::aim::graph;
use crate::node::Opaque;
use crate::node::{MainPass3dNode, prepare_core_3d_depth_textures};

pub const NAME: &str = "aim_parabolic";
pub mod input {
    pub const VIEW_ENTITY: &str = "view_entity";
}
pub mod node {
    pub const MAIN_PASS: &str = "main_pass";
    pub const UPSCALING: &str = "ups";
}

/// Creates a render graph for parabolic aim.
/// To interface, use AimPhase and NAME.
/// Add the pipeline plugin too.
pub struct Plugin;

impl bevy::app::Plugin for Plugin {
    fn build(&self, app: &mut App) {
        let render_app = match app.get_sub_app_mut(RenderApp) {
            Ok(render_app) => render_app,
            Err(_) => return,
        };

        render_app
            .init_resource::<DrawFunctions<AimPhase>>()
            .add_system_to_stage(RenderStage::Extract, extract_core_3d_camera_phases)
            .add_system_to_stage(RenderStage::Prepare, prepare_core_3d_depth_textures::<AimPhase>)
            .add_system_to_stage(RenderStage::PhaseSort, sort_phase_system::<AimPhase>);

        let pass_node_3d = MainPass3dNode::<ViewTarget, AimPhase>::new(&mut render_app.world);
        
        // This serves only to convert between buffer formats.
        let upscaling = UpscalingNode::new(&mut render_app.world);
        let mut graph = render_app.world.resource_mut::<RenderGraph>();

        let mut draw_3d_graph: RenderGraph =
        RenderGraph::default();
        draw_3d_graph.add_node(graph::node::MAIN_PASS, pass_node_3d);
        let input_node_id = draw_3d_graph.set_input(vec![SlotInfo::new(
            graph::input::VIEW_ENTITY,
            SlotType::Entity,
        )]);
        draw_3d_graph
            .add_slot_edge(
                input_node_id,
                graph::input::VIEW_ENTITY,
                graph::node::MAIN_PASS,
                MainPass3dNode::<ViewTarget, AimPhase>::IN_VIEW,
            )
            .unwrap();

        // Why can't I directly render to the "out texture" (screen)?
        // No one knows. maybe the screen is special,
        // and with raw textures it's possible to use the "out" target
        // in the main node.
        {
            draw_3d_graph.add_node(graph::node::UPSCALING, upscaling);
            draw_3d_graph
                .add_slot_edge(
                    input_node_id,
                    graph::input::VIEW_ENTITY,
                    graph::node::UPSCALING,
                    UpscalingNode::IN_VIEW,
                )
                .unwrap();
            // for correct sequencing
            draw_3d_graph
                .add_node_edge(
                    graph::node::MAIN_PASS,
                    graph::node::UPSCALING,
                ).unwrap();
        }
        graph.add_sub_graph(graph::NAME, dbg!(draw_3d_graph));
    }
}


pub fn extract_core_3d_camera_phases(
    mut commands: Commands,
    cameras_3d: Extract<Query<(Entity, &Camera), With<Camera3d>>>,
) {
    for (entity, camera) in &cameras_3d {
        if camera.is_active {
            commands.get_or_spawn(entity).insert((
                RenderPhase::<AimPhase>::default(),
            ));
        }
    }
}

pub struct Target;

pub type AimPhase = Opaque<Target>;
/* SPDX-License-Identifier: MIT or Apache-2.0
Based on Bevy
commit ea4aeff9ec8e204b8d4bc8cbb10c28e90fe271ff
file crates/bevy_pbr/src/wireframe.rs
*/
/*! A plugin for previewing from any angle the hit distribution of a projectile following a parabolic path.
*/

use bevy::math::Mat4;
use bevy::pbr::MeshPipeline;
use bevy::pbr::{DrawMesh, MeshPipelineKey, MeshUniform, SetMeshBindGroup, SetMeshViewBindGroup};
use bevy::asset::{AssetServer, Handle};
use bevy::core_pipeline::core_3d::Opaque3d;
use bevy::ecs::prelude::*;
use bevy::render::{
    mesh::{Mesh, MeshVertexBufferLayout},
    render_asset::RenderAssets,
    render_phase::{AddRenderCommand, DrawFunctions, RenderPhase, SetItemPipeline},
    render_resource::{
        PipelineCache, RenderPipelineDescriptor, Shader, SpecializedMeshPipeline,
        SpecializedMeshPipelineError, SpecializedMeshPipelines,
    },
    view::{ExtractedView, Msaa, VisibleEntities},
    RenderApp, RenderStage,
};
use bevy::utils::tracing::error;

use std::borrow::Cow;
use super::graph::PreviewPhase;
use crate::preview;

#[derive(Debug, Default)]
pub struct Plugin;

impl bevy::app::Plugin for Plugin {
    fn build(&self, app: &mut bevy::app::App) {
        if let Ok(render_app) = app.get_sub_app_mut(RenderApp) {
            render_app
                // can't make it work
                //.add_plugin(ExtractComponentPlugin::<preview::Camera>::default())
                .add_render_command::<PreviewPhase, Draw>()
                .init_resource::<Pipeline>()
                .init_resource::<SpecializedMeshPipelines<Pipeline>>()
                .insert_resource(ComponentUniforms::<ProjectileUniform>::default())
                .add_system_to_stage(RenderStage::Extract, extract_preview_targets)
                .add_system_to_stage(
                    RenderStage::Prepare,
                    prepare_projectile_uniforms
                )
                .add_system_to_stage(RenderStage::Queue, queue_meshes)
                .add_system_to_stage(RenderStage::Queue, queue_projectile_bind_group)
                .add_system_to_stage(RenderStage::Queue, queue_projectile_depth_bind_group);
        }
    }
}

use bevy::render::Extract;
use bevy::render::render_resource::BindGroupLayout;
use bevy::render::render_resource::BindGroupLayoutDescriptor;
use bevy::render::render_resource::{BindGroupLayoutEntry, BindingType, BufferBindingType, ShaderStages, TextureSampleType, TextureViewDimension, SamplerBindingType, Sampler, SamplerDescriptor, AddressMode, FilterMode, CompareFunction};
use bevy::render::render_resource::ShaderType;
use bevy::render::renderer::RenderDevice;


#[derive(Resource)]
pub struct Pipeline {
    mesh_pipeline: MeshPipeline,
    shader: Handle<Shader>,
    projectile_layout: BindGroupLayout,
    projectile_depth_layout: BindGroupLayout,
    projectile_depth_sampler: Sampler,
}

#[derive(Component, ShaderType, Clone)]
pub struct ProjectileUniform {
    pub linear_proj: Mat4,
    pub velocity: f32,
    pub gravity: f32,
}

fn extract_preview_targets(
    mut commands: Commands,
    views: Extract<Query<(Entity, &preview::Camera)>>,
) {
    for (e, cam) in views.iter() {
        commands.get_or_spawn(e).insert(cam.clone());
    }
}

use bevy::render::renderer::RenderQueue;
use std::marker::PhantomData;

// The version from extract_component doesn't expose enough of DynamicUniformBuffer to be useful.
/// This is a buffer for uniforms which will be populated with an array of values, each index holding that of a different instance of the component.
/// Later, the renderer will choose the correct index into this buffer when setting the bind group.
#[derive(Resource)]
pub struct ComponentUniforms<C: Component + ShaderType> {
    uniforms: DynamicUniformBuffer<C>,
}

impl<C: Component + ShaderType> Default for ComponentUniforms<C> {
    fn default() -> Self {
        Self {
            uniforms: Default::default(),
        }
    }
}

/// Stores the index of a uniform inside of [`ComponentUniforms`].
#[derive(Component)]
pub struct DynamicUniformIndex<C: Component> {
    index: u32,
    marker: PhantomData<C>,
}

/// This system writes the values into the bind group buffer.
/// They are transformed into uniforms and stored in the [`ComponentUniforms`] resource.
/// Depth textures are not touched here because they are already filled buffers.
fn prepare_projectile_uniforms(
    mut commands: Commands,
    render_device: Res<RenderDevice>,
    render_queue: Res<RenderQueue>,
    mut component_uniforms: ResMut<ComponentUniforms<ProjectileUniform>>,
    projectiles: Query<&ExtractedView, Without<preview::Camera>>,
    views: Query<(Entity, &preview::Camera)>,
) {
    component_uniforms.uniforms.clear();
    for (entity, preview::Camera(projectile_id)) in views.iter() {
        let projectile = projectiles.get(*projectile_id).unwrap();
        let projection = projectile.projection;
        let view = projectile.transform.compute_matrix();
        let inverse_view = view.inverse();
        let uniform = ProjectileUniform {
            linear_proj: projection * inverse_view,
            velocity: 10.0,
            gravity: 19.0,
        };
        let index = DynamicUniformIndex::<ProjectileUniform> {
            index: component_uniforms.uniforms.push(uniform.clone()),
            marker: PhantomData,
        };
        commands.get_or_spawn(entity).insert(index);
    }
    
    // is it even faster with 2 cameras?
    //commands.insert_or_spawn_batch(entities);

    component_uniforms
        .uniforms
        .write_buffer(&render_device, &render_queue);
}


impl FromWorld for Pipeline {
    fn from_world(render_world: &mut World) -> Self {
        let shader = render_world
            .resource::<AssetServer>()
            .load("shaders/preview.wgsl");
        let render_device = render_world.resource::<RenderDevice>();
        Self {
            mesh_pipeline: render_world.resource::<MeshPipeline>().clone(),
            shader,
            projectile_layout: render_device
                .create_bind_group_layout(&BindGroupLayoutDescriptor {
                    label: Some("projectile_layout"),
                    entries: &[BindGroupLayoutEntry {
                        binding: 0,
                        visibility: ShaderStages::VERTEX | ShaderStages::FRAGMENT,
                        ty: BindingType::Buffer {
                            ty: BufferBindingType::Uniform,
                            has_dynamic_offset: true,
                            min_binding_size: Some(ProjectileUniform::min_size()),
                        },
                        count: None,
                    }],
                }),
            projectile_depth_layout: render_device
                .create_bind_group_layout(&BindGroupLayoutDescriptor {
                    label: Some("projectile_depth_layout"),
                    entries: &[
                        // Ptojectile depth texture
                        BindGroupLayoutEntry {
                            binding: 0,
                            visibility: ShaderStages::FRAGMENT,
                            ty: BindingType::Texture {
                                multisampled: false,
                                sample_type: TextureSampleType::Depth,
                                view_dimension: TextureViewDimension::D2,
                            },
                            count: None,
                        },
                        // Ptojectile depth texture sampler
                        BindGroupLayoutEntry {
                            binding: 1,
                            visibility: ShaderStages::FRAGMENT,
                            ty: BindingType::Sampler(SamplerBindingType::Comparison),
                            count: None,
                        },
                    ],
                }),
            projectile_depth_sampler: render_device
                .create_sampler(&SamplerDescriptor {
                    address_mode_u: AddressMode::ClampToEdge,
                    address_mode_v: AddressMode::ClampToEdge,
                    address_mode_w: AddressMode::ClampToEdge,
                    mag_filter: FilterMode::Linear,
                    min_filter: FilterMode::Linear,
                    mipmap_filter: FilterMode::Nearest,
                    compare: Some(CompareFunction::GreaterEqual),
                    ..Default::default()
                }),
        }
    }
}

impl SpecializedMeshPipeline for Pipeline {
    type Key = MeshPipelineKey;

    fn specialize(
        &self,
        key: Self::Key,
        layout: &MeshVertexBufferLayout,
    ) -> Result<RenderPipelineDescriptor, SpecializedMeshPipelineError> {
        let mut descriptor = self.mesh_pipeline.specialize(key, layout)?;
        descriptor.vertex.shader = self.shader.clone_weak();
        // maybe here for accepting the target.output_texture() format:
        // descriptor.fragment.as_mut().unwrap.format = FOO;
        descriptor.fragment.as_mut().unwrap().shader = self.shader.clone_weak();
        descriptor.depth_stencil.as_mut().unwrap().bias.slope_scale = 1.0;
        descriptor.label = Some(Cow::from("preview_pipeline"));
        if let Some(ref mut l) = &mut descriptor.layout {
            l.push(self.projectile_layout.clone());
            l.push(self.projectile_depth_layout.clone());
        }
        Ok(descriptor)
    }
}

#[allow(clippy::too_many_arguments)]
fn queue_meshes(
    opaque_3d_draw_functions: Res<DrawFunctions<PreviewPhase>>,
    render_meshes: Res<RenderAssets<Mesh>>,
    wireframe_pipeline: Res<Pipeline>,
    mut pipelines: ResMut<SpecializedMeshPipelines<Pipeline>>,
    mut pipeline_cache: ResMut<PipelineCache>,
    msaa: Res<Msaa>,
    material_meshes: Query<
        (Entity, &Handle<Mesh>, &MeshUniform),
    >,
    mut views: Query<(&ExtractedView, &VisibleEntities, &mut RenderPhase<PreviewPhase>)>,
) {
    let draw_custom = opaque_3d_draw_functions
        .read()
        .get_id::<Draw>()
        .unwrap();
    let msaa_key = MeshPipelineKey::from_msaa_samples(msaa.samples);
    for (view, visible_entities, mut opaque_phase) in &mut views {
        let rangefinder = view.rangefinder3d();

        let view_key = msaa_key | MeshPipelineKey::from_hdr(view.hdr);
        let add_render_phase =
            |(entity, mesh_handle, mesh_uniform): (Entity, &Handle<Mesh>, &MeshUniform)| {
                if let Some(mesh) = render_meshes.get(mesh_handle) {
                    let key = view_key
                        | MeshPipelineKey::from_primitive_topology(mesh.primitive_topology);
                    let pipeline_id = pipelines.specialize(
                        &mut pipeline_cache,
                        &wireframe_pipeline,
                        key,
                        &mesh.layout,
                    );
                    let pipeline_id = match pipeline_id {
                        Ok(id) => id,
                        Err(err) => {
                            error!("{}", err);
                            return;
                        }
                    };
                    opaque_phase.add(PreviewPhase::new(Opaque3d {
                        entity,
                        pipeline: pipeline_id,
                        draw_function: draw_custom,
                        distance: rangefinder.distance(&mesh_uniform.transform),
                    }));
                }
            };

        let query = &material_meshes;
        visible_entities
            .entities
            .iter()
            .filter_map(|visible_entity| query.get(*visible_entity).ok())
            .for_each(add_render_phase);
    }
}

type Draw = (
// TODO: give the shader info about projectile motion:
// velocity, gravity. Probably the camera should carry this info best.
    SetItemPipeline,
    SetMeshViewBindGroup<0>,
    SetMeshBindGroup<1>,
    SetProjectileBindGroups<2, 3>,
    DrawMesh,
);


use bevy::ecs::system::SystemParamItem;
use bevy::ecs::system::lifetimeless::Read;
use bevy::ecs::system::lifetimeless::{SQuery, SRes};
use bevy::render::render_phase::EntityRenderCommand;
use bevy::render::render_phase::RenderCommandResult;
use bevy::render::render_phase::TrackedRenderPass;
use bevy::render::render_resource::BindGroup;
use bevy::render::render_resource::BindGroupDescriptor;
use bevy::render::render_resource::{BindGroupEntry, DynamicUniformBuffer};

// a bind group is just a place for data it seems.
// it may be a resource or a component.
// Here, the projectile uniform is placed on projectile aim camera.

// We'll have to select the projectile camera via an entity pointer on the current camera, and then queue the uniform for each mesh rendered in this pipeline.

use bevy::render::render_resource::BindingResource;
use crate::node::ViewDepthTexture;

#[derive(Resource)]
struct ProjectileBindGroup(BindGroup);

fn queue_projectile_bind_group(
    mut commands: Commands,
    aim_pipeline: Res<Pipeline>,
    render_device: Res<RenderDevice>,
    projectile_uniforms: Res<ComponentUniforms<ProjectileUniform>>,
) {
    if let Some(projectile_binding) = projectile_uniforms.uniforms.binding() {
        let bind_group = ProjectileBindGroup(
            render_device.create_bind_group(&BindGroupDescriptor {
                entries: &[BindGroupEntry {
                    binding: 0,
                    resource: projectile_binding.clone(),
                }],
                label: Some("projectile_bind_group"),
                layout: &aim_pipeline.projectile_layout,
            })
        );
        commands.insert_resource(bind_group);
    }
}

// Different approach: because a texture handle gets embedded directly into the bind group, rather than being indexed (maybe it could but I won't do it), we create a separate bind group for every component.
// Because there's only a few cameras, no one should care.
#[derive(Component)]
struct ProjectileDepthBindGroup(BindGroup);

fn queue_projectile_depth_bind_group(
    mut commands: Commands,
    pipeline: Res<Pipeline>,
    render_device: Res<RenderDevice>,
    projectiles: Query<&ViewDepthTexture>,
    cameras: Query<(Entity, &preview::Camera)>,
) {
    let depths = cameras.iter().map(|(e, preview::Camera(projectile_id))|
        (e, projectiles.get(*projectile_id).unwrap())
    );
    for (e, depth) in depths {
        let bind_group = ProjectileDepthBindGroup(
            render_device.create_bind_group(&BindGroupDescriptor {
                entries: &[
                    BindGroupEntry {
                        binding: 0,
                        resource: BindingResource::TextureView(&depth.0.view),
                    },
                    BindGroupEntry {
                        binding: 1,
                        resource: BindingResource::Sampler(
                            &pipeline.projectile_depth_sampler,
                        ),
                    },
                ],
                label: Some("projectile_depth_bind_group"),
                layout: &pipeline.projectile_depth_layout,
            })
        );
        commands.get_or_spawn(e).insert(bind_group);
    }
}


// This is called for each entity, to apply the bind group buffer.
// The bind group buffer contains data for all entities, so the query serves to find the index into the buffer for the current entity.
// This index is passed to the bind group selection command.
struct SetProjectileBindGroups<const I: usize, const J: usize>;

impl<const I: usize, const J: usize> EntityRenderCommand for SetProjectileBindGroups<I, J> {
    type Param = (
        SRes<ProjectileBindGroup>,
        SQuery<(
            Read<DynamicUniformIndex<ProjectileUniform>>,
            Read<ProjectileDepthBindGroup>,
        )>,
    );
    
    #[inline]
    fn render<'w>(
        view: Entity,
        _item: Entity,
        (bind_group, query): SystemParamItem<'w, '_, Self::Param>,
        pass: &mut TrackedRenderPass<'w>,
    ) -> RenderCommandResult {
        let (target, depth) = query.get_inner(view).unwrap();
        pass.set_bind_group(I, &bind_group.into_inner().0, &[target.index]);
        pass.set_bind_group(J, &depth.0, &[]);
        RenderCommandResult::Success
    }
}

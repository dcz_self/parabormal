pub mod graph;
pub mod pipeline;

use bevy::ecs::component::Component;
use bevy::ecs::entity::Entity;
use bevy::ecs::query::{QueryItem, With};
use bevy::render::extract_component::ExtractComponent;


#[derive(Component, Clone, Debug)]
pub struct Camera(pub Entity);

impl ExtractComponent for Camera {
    type Query = &'static Self;
    // What is "don't filter"?
    type Filter = With<Camera>;

    fn extract_component(item: QueryItem<'_, Self::Query>) -> Self {
        item.clone()
    }
}
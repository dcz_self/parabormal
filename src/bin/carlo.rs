use rand;
use rand_distr::{Normal, Distribution};

///  {\displaystyle \Phi (x)={\frac {1}{2}}+\varphi (x)\left(x+{\frac {x^{3}}{3}}+{\frac {x^{5}}{3\cdot 5}}+{\frac {x^{7}}{3\cdot 5\cdot 7}}+{\frac {x^{9}}{3\cdot 5\cdot 7\cdot 9}}+\cdots \right)}

/// For Mahalanobis (here devolves to Euclidean) distance r:
/// F (r) = 1 − e^(−r^2/2).
/// (actually checks out)
///
/// For estimating quantiles:
/// r = sqrt( −2 ln(1 − p))
///
/// Some useful values for estimating shot accuracy, expressed in single-dimensional sigmas
/// >>> quan(0.5) # coin flip
/// 1.1774100225154747
/// >>> quan(0.95) # mostly hit
/// 2.447746830680816
/// >>> quan(0.25) # you sure?
/// 0.7585276164409321
/// >>> quan(0.1) # don't waste time
/// 0.4590436050264207
///
/// from N-DIMENSIONAL CUMULATIVE FUNCTION, AND OTHER USEFUL
/// FACTS ABOUT GAUSSIANS AND NORMAL DENSITIES
/// by Michaël Bensimhoun (reviewed by David Arnon)
/// Jerusalem, 06/2009

fn main() {
    const TOTAL: u64 = 100_000_000;
    let mut onesigma_count = 0;
    let mut twosigma_count = 0;
    let mut threesigma_count = 0;
    for i in 0..TOTAL {
        // mean 0, standard deviation 1
        let normal = Normal::new(0.0, 1.0).unwrap();
        let x: f32 = normal.sample(&mut rand::thread_rng());
        let y = normal.sample(&mut rand::thread_rng());

        let dot = x*x + y*y;
        if dot < 1.0 { onesigma_count += 1 }
        if dot < 2.0 * 2.0 { twosigma_count += 1 }
        if dot < 3.0 * 3.0 { threesigma_count += 1 }
        if i % 1000_000 == 0 {
            println!("{}", i / 1000_000);
        }
    }
    println!(
        "{0:.5} {1:.5} {2:.5}",
        //  0.39347 0.86459 0.98889
        // it's not sigma! Distance sigma becomes different from each variable sigma
        onesigma_count as f64 / TOTAL as f64,
        twosigma_count as f64 / TOTAL as f64,
        threesigma_count as f64 / TOTAL as f64,
    );
}
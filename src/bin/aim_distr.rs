//! Estimates projectile accuracy over a parabolic path

use bevy::core_pipeline::clear_color::ClearColorConfig;
use bevy::{
    prelude::*,
    reflect::TypeUuid,
    render::{
        RenderApp,
        RenderStage,
        render_resource::{
            AsBindGroup, BindingType, Buffer, BufferBindingType, BufferDescriptor, BufferUsages, BufferSlice, ShaderRef, ShaderStages, TextureSampleType,
            Extent3d, TextureDescriptor, TextureDimension, TextureFormat,
            TextureUsages, TextureViewDimension,
        },
    },
    render::render_asset::RenderAssets,
};
use bevy::render::camera::CameraRenderGraph;
use bevy::render::camera::RenderTarget;
use bevy::render::extract_resource::{ExtractResource, ExtractResourcePlugin};
use bevy::render::render_graph;
use bevy::render::render_graph::RenderGraph;
use bevy::render::render_resource::{BindGroup, BindGroupDescriptor, BindGroupEntry, BindGroupLayout, BindGroupLayoutEntry, BindGroupLayoutDescriptor, BindingResource, CachedPipelineState, CachedComputePipelineId, ComputePassDescriptor, ComputePipelineDescriptor, MapMode, PipelineCache, ShaderType};
use bevy::render::renderer::{ RenderContext, RenderDevice};
use bevy::render::view::RenderLayers;
use bevy::window::{CreateWindow, WindowId};
use parabormal::aim;
use parabormal::aim::Camera as AimCamera;
use parabormal::preview;
use std::borrow::Cow;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};
use wgpu::Maintain;


fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .insert_resource(Msaa { samples: 1 })
        .add_plugin(MaterialPlugin::<BullseyeMaterial>::default())
        .add_startup_system(setup)
        .add_system(switch_layers)
        //.add_system(switch_cameras)
        //.add_system(find_center_of_mass)
        .add_system(read_result)
        .add_plugin(ComputePlugin)
        .add_plugin(aim::graph::Plugin)
        .add_plugin(aim::pipeline::Plugin)
        .add_plugin(preview::graph::Plugin)
        .add_plugin(preview::pipeline::Plugin)
        .run();
}

fn get_camera_pos() -> Vec3 {
    Vec3{ x: -5.0, y: 1.5, z: 6.0 }
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut smaterials: ResMut<Assets<StandardMaterial>>,
    mut materials: ResMut<Assets<BullseyeMaterial>>,
    mut images: ResMut<Assets<Image>>,
    mut create_window_events: EventWriter<CreateWindow>,
) {

    let camera_pos = get_camera_pos();
    // TODO: remove, obsoleted by custom render graphs
    // Display
    let user_layer = RenderLayers::layer(0);
    // Calculate
    let accuracy_layer = RenderLayers::layer(1);
    // View bullseye
    let accuracy_view_layer = RenderLayers::layer(2);
    
   
    let nontarget_material = BullseyeMaterial {
        warp_enable: 1,
        warp_origin: camera_pos,
        target: 0,
    };
    let nontarget_material = materials.add(nontarget_material);
    
    let target_material = BullseyeMaterial {
        warp_enable: 1,
        warp_origin: camera_pos,
        target: 1,
    };
    let target_material = materials.add(target_material);

    // Dynamic scenes will not really work, but I can't be arsed.
    let mut spawn_trio = |
        mesh: Handle<Mesh>,
        transform,
        material,
        target,
    | {
        commands.spawn(PbrBundle {
            mesh: mesh.clone(),
            material,
            transform,
            ..default()
        });
        commands.spawn((
            MaterialMeshBundle {
                mesh: mesh.clone(),
                transform,
                material: match target {
                    false => nontarget_material.clone(),
                    true => target_material.clone(),
                },
                ..default()
            },
            accuracy_layer,
        ));
        commands.spawn((
            MaterialMeshBundle {
                mesh: mesh.clone(),
                transform,
                material: nontarget_material.clone(),
                ..default()
            },
            accuracy_view_layer,
        ));
    };
    
    let capsule_mesh = meshes.add(Mesh::from(shape::Capsule::default()));
    let capsule_transform = Transform::from_xyz(0.0, 0.5, 1.0);
    
    spawn_trio(
        capsule_mesh, 
        capsule_transform, 
        smaterials.add(Color::BLUE.into()), 
        false,
    );
    
    commands.spawn(PointLightBundle {
        transform: Transform::from_translation(Vec3::new(0.0, 0.0, 10.0)),
        ..default()
    });
    
    let mesh = Mesh::from(shape::Cube { size: 1.0 });
    let mesh = meshes.add(mesh);

    // cubes
    for i in 0..10 {
        let transform = Transform::from_xyz(0.0, 0.5, -2.0 * i as f32);
        
        let pretty_target_material = smaterials.add(Color::RED.into());
        let pretty_obstacle_material = smaterials.add(Color::BLUE.into());
        
        let mut m = commands
            .spawn(MaterialMeshBundle {
                mesh: mesh.clone(),
                transform,
                material: {
                    if i < 5 {
                        pretty_target_material
                    } else {
                        pretty_obstacle_material 
                    }
                    .clone()
                },
                ..default()
            });
        if i < 5 {
            m.insert(aim::Target);
        }
        
        commands
            .spawn((
                MaterialMeshBundle {
                    mesh: mesh.clone(),
                    transform,
                    material: {
                        if i < 5 {
                            target_material.clone()
                        } else {
                            nontarget_material.clone()
                        }
                    },
                    ..default()
                },
                accuracy_layer,
            ));
    }


    let size = Extent3d {
        width: 400,
        height: 400,
        ..default()
    };

    // This is the texture that will be rendered to.
    let mut image = Image {
        texture_descriptor: TextureDescriptor {
            label: None,
            size,
            dimension: TextureDimension::D2,
            format: TextureFormat::Bgra8UnormSrgb,
            mip_level_count: 1,
            sample_count: 1,
            usage: TextureUsages::TEXTURE_BINDING
                | TextureUsages::COPY_DST
                | TextureUsages::RENDER_ATTACHMENT,
        },
        ..default()
    };
    
    image.resize(size);
    
    let image_handle = images.add(image);
    commands.insert_resource(AccuracyImage(image_handle.clone()));
    commands.insert_resource(UpdateRequest::NoUpdate);
    commands.insert_resource(FreshDataReady(Arc::new(AtomicBool::new(false))));

    // custom graph accuracy estimation debug view
    // separate window cause switching doesn't work with intel 4000
    let window_id = WindowId::new();

    // sends out a "CreateWindow" event, which will be received by the windowing backend
    create_window_events.send(CreateWindow {
        id: window_id,
        descriptor: WindowDescriptor {
            width: 400.,
            height: 400.,
            title: "Debug window".to_string(),
            ..default()
        },
    });
    
    let preview_window_id = WindowId::new();
    create_window_events.send(CreateWindow {
        id: preview_window_id,
        descriptor: WindowDescriptor {
            width: 400.,
            height: 400.,
            title: "Preview window".to_string(),
            ..default()
        },
    });
    
    
    let make_aim_cambundle = |transform, target| (
        Camera3dBundle {
            camera_render_graph: CameraRenderGraph::new(aim::graph::NAME),
            transform,
            camera_3d: Camera3d {
                clear_color: ClearColorConfig::Custom(Color::BLACK),
                ..default()
            },
            camera: Camera {
                target,
                ..default()
            },
            ..default()
        },
    );
    
    // custom graph accuracy estimation 
    let aim_camera_id = commands
        .spawn((
            make_aim_cambundle(
                Transform::from_translation(get_camera_pos() + Vec3::new(4.0, 0.0, 0.0))
                    .looking_at(Vec3::ZERO, Vec3::Y),
                RenderTarget::Image(image_handle.clone())
            ),
            AimCamera,
        ))
        .with_children(|b| {
            b.spawn((
                make_aim_cambundle(
                    Transform::default(),
                    RenderTarget::Window(window_id)
                ),
            ));
        })
        .id();

    // pretty scene camera
    commands
        .spawn((
            Camera3dBundle {
                transform: Transform::from_xyz(camera_pos.x, camera_pos.y, camera_pos.z)
                    .looking_at(Vec3::ZERO, Vec3::Y),
                camera_3d: Camera3d {
                    clear_color: ClearColorConfig::Custom(Color::TEAL),
                    ..default()
                },
                ..default()
            },
            user_layer,
            UserCamera,
        ))
        // preview camera locked to pretty in a separate window
        // because Intel driver makes switching at runtime impossible
        .with_children(|b| {
            b.spawn((
                Camera3dBundle {
                    camera_render_graph: CameraRenderGraph::new(preview::graph::NAME),
                    transform: Transform::default(),
                    camera_3d: Camera3d {
                        clear_color: ClearColorConfig::Custom(Color::TEAL),
                        ..default()
                    },
                    camera: Camera {
                        target: RenderTarget::Window(preview_window_id),
                        ..default()
                    },
                    ..default()
                },
                preview::Camera(aim_camera_id),
            ));
        });
}

#[derive(Clone, Deref, Resource, ExtractResource)]
struct AccuracyImage(Handle<Image>);

/// Monochromatic material, colored based on visibility or spread
/// Use only with AccuracyPipeline
#[derive(AsBindGroup, Debug, Clone, TypeUuid)]
#[uuid = "f690fdae-d598-45ab-8225-97e2a3f056e0"]
pub struct BullseyeMaterial {
    /// Unused
    #[uniform(0)]
    warp_enable: u32,
    /// Projectile source
    #[uniform(1)]
    warp_origin: Vec3,
    /// This mesh is colored as a hit target
    #[uniform(2)]
    target: u32,
}

impl Material for BullseyeMaterial {
    fn vertex_shader() -> ShaderRef {
        "shaders/accuracy_view.wgsl".into()
    }
    fn fragment_shader() -> ShaderRef {
        "shaders/accuracy_view.wgsl".into()
    }
}

#[derive(Resource, Clone, ExtractResource)]
struct OutBuffer {
    verticals: Buffer,
    horizontals: Buffer,
}

pub struct ComputePlugin;

impl Plugin for ComputePlugin {
    fn build(&self, app: &mut App) {
        app
            .add_plugin(ExtractResourcePlugin::<UpdateRequest>::default())
            .add_plugin(ExtractResourcePlugin::<OutBuffer>::default())
            .add_plugin(ExtractResourcePlugin::<FreshDataReady>::default())
            .add_plugin(ExtractResourcePlugin::<AccuracyImage>::default());
            
        
        let render_device = app.world.resource::<RenderDevice>();
        let buffer = OutBuffer {
            verticals: render_device.create_buffer(&BufferDescriptor {
                label: Some("verts"),
                size: std::mem::size_of::<f32>() as u64 * 128,
                usage: BufferUsages::STORAGE | BufferUsages::MAP_READ,
                mapped_at_creation: false,
            }),
            horizontals: render_device.create_buffer(&BufferDescriptor {
                label: Some("horzs"),
                size: std::mem::size_of::<f32>() as u64 * 128,
                usage: BufferUsages::STORAGE | BufferUsages::MAP_READ,
                mapped_at_creation: false,
            }),
        };
        
        app.insert_resource(buffer);
        
        let render_app = app.sub_app_mut(RenderApp);
        render_app
            .init_resource::<AccuracyPipeline>()
            .add_system_to_stage(RenderStage::Queue, queue_bind_group);

        let mut render_graph = render_app.world.resource_mut::<RenderGraph>();
        render_graph.add_node("accuracy", AccuracyNode::default());
        // Not sure what this is needed for
        render_graph
            .add_node_edge(
                bevy::render::main_graph::node::CAMERA_DRIVER,
                "accuracy",
            )
            .unwrap();
    }
}

// TODO: actually use this as a bind group
#[derive(Clone, ShaderType)]
struct AccuracyParamsUniform {
    shot_source: Vec3,
    shot_velocity: f32,
}

#[derive(Resource)]
struct CentroidBindGroup(BindGroup);

fn queue_bind_group(
    mut commands: Commands,
    pipeline: Res<AccuracyPipeline>,
    gpu_images: Res<RenderAssets<Image>>,
    accuracy_image: Res<AccuracyImage>,
    render_device: Res<RenderDevice>,
    outputs: Res<OutBuffer>,
) {
    let view = &gpu_images[&accuracy_image.0];
    let bind_group = render_device.create_bind_group(&BindGroupDescriptor {
        label: None,
        layout: &pipeline.layout,
        entries: &[
            BindGroupEntry {
                binding: 0,
                resource: BindingResource::TextureView(&view.texture_view),
            },
            BindGroupEntry {
                binding: 1,
                resource: BindingResource::Buffer(outputs.verticals.as_entire_buffer_binding()),
            },
            BindGroupEntry {
                binding: 2,
                resource: BindingResource::Buffer(outputs.horizontals.as_entire_buffer_binding()),
            },
        ],
    });
    commands.insert_resource(CentroidBindGroup(bind_group));
}

#[derive(Resource)]
pub struct AccuracyPipeline {
    pipeline: CachedComputePipelineId,
    layout: BindGroupLayout,
}

impl FromWorld for AccuracyPipeline {
    fn from_world(world: &mut World) -> Self {
        let layout = world
            .resource::<RenderDevice>()
            .create_bind_group_layout(&BindGroupLayoutDescriptor {
                label: None,
                entries: &[
                    // texture
                    BindGroupLayoutEntry {
                        binding: 0,
                        visibility: ShaderStages::COMPUTE,
                        ty: BindingType::Texture {
                            multisampled: false,
                            sample_type: TextureSampleType::Float { filterable: false },
                            view_dimension: TextureViewDimension::D2,
                        },
                        count: None,
                    },
                    // verticals
                    BindGroupLayoutEntry {
                        binding: 1,
                        visibility: ShaderStages::COMPUTE,
                        ty: BindingType::Buffer {
                            ty: BufferBindingType::Storage { read_only: false },
                            has_dynamic_offset: false,
                            min_binding_size: None,
                        },
                        count: None,
                    },
                    // horizontals
                    BindGroupLayoutEntry {
                        binding: 2,
                        visibility: ShaderStages::COMPUTE,
                        ty: BindingType::Buffer {
                            ty: BufferBindingType::Storage { read_only: false },
                            has_dynamic_offset: false,
                            min_binding_size: None,
                        },
                        count: None,
                    },
                ],
            });
        let shader = world
            .resource::<AssetServer>()
            .load("shaders/sum_sides.wgsl");
        let mut pipeline_cache = world.resource_mut::<PipelineCache>();
        let pipeline = pipeline_cache.queue_compute_pipeline(ComputePipelineDescriptor {
            label: None,
            layout: Some(vec![layout.clone()]),
            shader: shader.clone(),
            shader_defs: vec![],
            entry_point: Cow::from("compute"),
        });
        Self {
            pipeline,
            layout,
        }
    }
}

enum AccuracyState {
    Loading,
    Calculate,
    ReadResult,
    Idle,
}

struct AccuracyNode {
    state: AccuracyState,
}

impl Default for AccuracyNode {
    fn default() -> Self {
        Self {
            state: AccuracyState::Loading,
        }
    }
}

impl render_graph::Node for AccuracyNode {
    fn update(&mut self, world: &mut World) {
        let pipeline = world.resource::<AccuracyPipeline>();
        let pipeline_cache = world.resource::<PipelineCache>();
        let request = world.resource::<UpdateRequest>();
        let mut read_ready = false;

        // if the corresponding pipeline has loaded, transition to the next stage
        let request = match self.state {
            AccuracyState::Loading => {
                if let CachedPipelineState::Ok(_) =
                    pipeline_cache.get_compute_pipeline_state(pipeline.pipeline)
                {
                    self.state = AccuracyState::Idle;
                }
                None
            }
            AccuracyState::Calculate => {
                self.state = AccuracyState::ReadResult;
                None
            },
            AccuracyState::ReadResult => {
                self.state = AccuracyState::Idle;
                read_ready = true;
                None
            },
            AccuracyState::Idle => match *request {
                UpdateRequest::UpdateNow => {
                    self.state = AccuracyState::Calculate;
                    Some(UpdateRequest::NoUpdate)
                },
                UpdateRequest::NoUpdate => None,
            },
        };
        if let Some(request) = request {
            *world.resource_mut::<UpdateRequest>() = request;
        }
        if read_ready {
            world.resource::<FreshDataReady>().0.store(true, Ordering::Relaxed);
        }
    }

    fn run(
        &self,
        _graph: &mut render_graph::RenderGraphContext,
        render_context: &mut RenderContext,
        world: &World,
    ) -> Result<(), render_graph::NodeRunError> {
        let bind_group = &world.resource::<CentroidBindGroup>().0;
        let pipeline_cache = world.resource::<PipelineCache>();
        let pipeline = world.resource::<AccuracyPipeline>();

        let mut pass = render_context
            .command_encoder
            .begin_compute_pass(&ComputePassDescriptor::default());

        pass.set_bind_group(0, bind_group, &[]);

        // select the pipeline based on the current state
        match self.state {
            AccuracyState::Loading | AccuracyState::Idle => {}
            AccuracyState::Calculate => {
                let update_pipeline = pipeline_cache
                    .get_compute_pipeline(pipeline.pipeline)
                    .unwrap();
                pass.set_pipeline(update_pipeline);
                pass.dispatch_workgroups(1, 1, 1);
            },
            AccuracyState::ReadResult => {}
        }

        Ok(())
    }
}

fn read_result(
    ready: Res<FreshDataReady>,
    buf: Res<OutBuffer>,
    device: Res<RenderDevice>,
    mut cameras: Query<
        (&mut Transform, &mut GlobalTransform, &mut Camera),
        With<AimCamera>
    >,
    mut user_cameras: Query<
        &mut Transform,
        (With<UserCamera>, Without<AimCamera>),
    >,
) {
    let mut center = None;
    if let true = ready.0.load(Ordering::Relaxed) {
        read_buffers(
            &*buf,
            &*device,
            |verts, horzs| {
                fn calc_center(weights: &[f32]) -> Option<f32> {
                    let total: f32 = weights.iter().sum();
                    if total > 0.0 {
                        Some(weights.iter()
                            .enumerate()
                            .map(|(i, weight)| i as f32 * weight)
                            .sum::<f32>()
                            / total)
                    } else { None }
                }
                // The center column is the horizontal center.
                if let (Some(hc), Some(vc)) = (calc_center(verts), calc_center(horzs)) {
                    center = Some((hc, vc));
                }
            },
        );
        if let Some(center) = center {
            for (mut position, transform, camera) in cameras.iter_mut() {
                if let Some(ray) = camera.viewport_to_world(
                    &*transform,
                    // y coord is reversed
                    Vec2::new(center.0, 128.0 - center.1),
                ) {
                    let up = position.up();
                    position.look_at(
                        ray.origin + ray.direction,
                        up,
                    );
                    for mut pos in user_cameras.iter_mut() {
                        *pos = *position;
                    }
                }
            }
        }
        ready.0.store(false, Ordering::Relaxed);
    }
}

fn read_buffers(
    buf: &OutBuffer,
    device: &RenderDevice,
    f: impl FnOnce(&[f32], &[f32]),
) {
    let vslice = buf.verticals.slice(..);
    vslice.map_async(MapMode::Read, move |result| {
        let err = result.err();
        if err.is_some() {
            panic!("{}", err.unwrap().to_string());
        }
    });
    
    let hslice = buf.horizontals.slice(..);
    hslice.map_async(MapMode::Read, move |result| {
        let err = result.err();
        if err.is_some() {
            panic!("{}", err.unwrap().to_string());
        }
    });
    
    let device = device.wgpu_device();
    device.poll(Maintain::Wait);

    fn read(slice: BufferSlice, count: usize, f: impl FnOnce(&[f32])) {
        let data = slice.get_mapped_range();

        let bufptr = data.as_ptr() as *const f32;
        let bufdata = unsafe { std::slice::from_raw_parts(bufptr, count) };
        f(bufdata);
        drop(data);
    }

    read(vslice, 128, |vbuf| read(hslice, 128, |hbuf| f(vbuf, hbuf)));
    
    buf.verticals.unmap();
    buf.horizontals.unmap();            
}

#[derive(Resource, Clone, ExtractResource)]
enum UpdateRequest {
    UpdateNow,
    NoUpdate,
}

#[derive(Component)]
struct UserCamera;


fn switch_layers(
    mouse_button_input: Res<Input<MouseButton>>,
    mut update_request: ResMut<UpdateRequest>,
    mut cameras: Query<&mut RenderLayers, With<UserCamera>>,
) {
    if mouse_button_input.just_pressed(MouseButton::Left) {
        *update_request = UpdateRequest::UpdateNow;
    }
    if mouse_button_input.just_pressed(MouseButton::Right) {
        for mut layer in cameras.iter_mut() {
            *layer = RenderLayers::layer(1);
        }
    }
    if mouse_button_input.just_released(MouseButton::Right) {
        for mut layer in cameras.iter_mut() {
            *layer = RenderLayers::layer(0);
        }
    }
}

/* Doesn't work on Intel
fn switch_cameras(
    mut commands: Commands,
    mouse_button_input: Res<Input<MouseButton>>,
    preview_cameras: Query<Entity, With<preview::Camera>>,
) {
    if mouse_button_input.just_pressed(MouseButton::Middle) {
        commands.spawn((
            Camera3dBundle {
                camera_render_graph: CameraRenderGraph::new(aim::graph::NAME),
                transform: Transform::from_translation(-get_camera_pos())
                    .looking_at(Vec3::ZERO, Vec3::Y),
                camera_3d: Camera3d {
                    clear_color: ClearColorConfig::Custom(Color::BLACK),
                    ..default()
                },
                camera: Camera {
                    priority: 1,
                    ..default()
                },
                ..default()
            },
            //accuracy_layer,
            RenderLayers::layer(0),
            preview::Camera,
        ));
    }
    if mouse_button_input.just_released(MouseButton::Middle) {
        for e in preview_cameras.iter() {
            dbg!(e);
            commands.entity(e).despawn();
        }
    }
}
*/
#[derive(Resource, Clone, Debug, ExtractResource)]
struct FreshDataReady(Arc<AtomicBool>);
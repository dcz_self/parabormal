/* SPDX-License-Identifier: MIT or Apache-2.0
Based on Bevy
commit ea4aeff9ec8e204b8d4bc8cbb10c28e90fe271ff
file examples/3d/wireframe.rs
*/
//! Showcases locating

use bevy::prelude::*;
use bevy::render::camera::CameraRenderGraph;
use parabormal::camera;
use parabormal::plugin3d;

fn main() {
    App::new()
    // this is for the scaling node I think
        .insert_resource(Msaa { samples: 1 })
        .add_plugins(DefaultPlugins)
        .add_plugin(camera::Plugin)
        .add_plugin(plugin3d::WireframePlugin)
        .add_startup_system(setup)
        .add_system(rotate)
        .run();
}

/// set up a simple 3D scene
fn setup(
    mut commands: Commands,
    mut wireframe_config: ResMut<plugin3d::WireframeConfig>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    // plane
    commands.spawn(PbrBundle {
        mesh: meshes.add(Mesh::from(shape::Plane { size: 5.0 })),
        material: materials.add(Color::rgb(0.3, 0.5, 0.3).into()),
        ..default()
    });
    // cube
    commands.spawn((
        PbrBundle {
            mesh: meshes.add(Mesh::from(shape::Cube { size: 1.0 })),
            material: materials.add(Color::rgb(0.8, 0.7, 0.6).into()),
            transform: Transform::from_xyz(0.0, 0.5, 0.0),
            ..default()
        },
        // This enables wireframe drawing on this entity
        plugin3d::NoWireframe,
    ));
    // camera
    commands.spawn(Camera3dBundle {
        camera_render_graph: CameraRenderGraph::new(camera::graph::NAME),
        transform: Transform::from_xyz(-2.0, 2.5, 5.0).looking_at(Vec3::ZERO, Vec3::Y),
        ..default()
    });
}

fn rotate(
    mut meshes: Query<&mut Transform, With<Handle<StandardMaterial>>>,
) {
    for mut t in meshes.iter_mut() {
        t.rotate_axis(Vec3::new(0.0, 1.0, 0.0), 5.0);
    }
}
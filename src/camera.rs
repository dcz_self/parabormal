/*! Special purpose camera without the core_3d pass */

use bevy::app::App;
use bevy::core_pipeline::core_3d::Camera3d;
use bevy::core_pipeline::upscaling::UpscalingNode;
use bevy::prelude::{Bundle, Component};
use bevy::render::{Extract, RenderApp, RenderStage};
use bevy::render::camera::{ Camera, CameraRenderGraph, Projection };
use bevy::render::primitives::Frustum;
use bevy::render::render_graph::{RenderGraph, SlotInfo, SlotType};
use bevy::render::render_phase::{DrawFunctions, RenderPhase, sort_phase_system};
use bevy::render::view::{ViewTarget, VisibleEntities};
use bevy::transform::components::{GlobalTransform, Transform};
use crate::node::{MainPass3dNode, Opaque};
use crate::plugin3d::Position;

pub mod graph {
    pub const NAME: &str = "special_3d";
    pub mod input {
        pub const VIEW_ENTITY: &str = "view_entity";
    }
    pub mod node {
        pub const MAIN_PASS: &str = "main_pass";
        pub const UPSCALING: &str = "ups";
    }
}

// Do we need a special camera? The core 3d pass should recognize that we have a separate render graph
#[derive(Default, Component)]
pub struct SpecialCamera {
    camera: Camera3d,
}

#[derive(Bundle)]
pub struct CameraBundle {
    pub camera: Camera,
    pub camera_render_graph: CameraRenderGraph,
    pub projection: Projection,
    pub visible_entities: VisibleEntities,
    pub frustum: Frustum,
    pub transform: Transform,
    pub global_transform: GlobalTransform,
    pub special_camera: SpecialCamera,
}

// NOTE: ideally Perspective and Orthographic defaults can share the same impl, but sadly it breaks rust's type inference
impl Default for CameraBundle {
    fn default() -> Self {
        Self {
            camera_render_graph: CameraRenderGraph::new(graph::NAME),
            camera: Default::default(),
            projection: Default::default(),
            visible_entities: Default::default(),
            frustum: Default::default(),
            transform: Default::default(),
            global_transform: Default::default(),
            special_camera: Default::default(),
        }
    }
}

use bevy::ecs::system::{Commands, Query};
use bevy::ecs::entity::Entity;
use bevy::ecs::query::With;

fn check(
    mut commands: Commands,
    q: Query<Entity, With<Camera3d>>,
) {
    for q in q.iter() {
        commands.entity(q).log_components();
    }
}

pub struct Plugin;

impl bevy::app::Plugin for Plugin {
    fn build(&self, app: &mut App) {
        let render_app = match app.get_sub_app_mut(RenderApp) {
            Ok(render_app) => render_app,
            Err(_) => return,
        };

        render_app
            .init_resource::<DrawFunctions<Opaque<Position>>>()
            .add_system_to_stage(RenderStage::Extract, extract_core_3d_camera_phases)
            .add_system_to_stage(RenderStage::PhaseSort, sort_phase_system::<Opaque<Position>>);

        let pass_node_3d = MainPass3dNode::<ViewTarget, Opaque<Position>>::new(&mut render_app.world);
        let upscaling = UpscalingNode::new(&mut render_app.world);
        let mut graph = render_app.world.resource_mut::<RenderGraph>();

        let mut draw_3d_graph: RenderGraph =
        RenderGraph::default();
        draw_3d_graph.add_node(graph::node::MAIN_PASS, pass_node_3d);
        let input_node_id = draw_3d_graph.set_input(vec![SlotInfo::new(
            graph::input::VIEW_ENTITY,
            SlotType::Entity,
        )]);
        draw_3d_graph
            .add_slot_edge(
                input_node_id,
                graph::input::VIEW_ENTITY,
                graph::node::MAIN_PASS,
                MainPass3dNode::<ViewTarget, Opaque<Position>>::IN_VIEW,
            )
            .unwrap();

        // Why can't I directly render to the "out texture" (screen)?
        // No one knows. maybe the screen is special,
        // and with raw textures it's possible to use the "out" target
        // in the main node.
        {
            draw_3d_graph.add_node(graph::node::UPSCALING, upscaling);
            draw_3d_graph
                .add_slot_edge(
                    input_node_id,
                    graph::input::VIEW_ENTITY,
                    graph::node::UPSCALING,
                    UpscalingNode::IN_VIEW,
                )
                .unwrap();
            // for correct sequencing
            draw_3d_graph
                .add_node_edge(
                    graph::node::MAIN_PASS,
                    graph::node::UPSCALING,
                ).unwrap();
        }
        graph.add_sub_graph(graph::NAME, dbg!(draw_3d_graph));
    }
}

pub fn extract_core_3d_camera_phases(
    mut commands: Commands,
    cameras_3d: Extract<Query<(Entity, &Camera), With<Camera3d>>>,
) {
    for (entity, camera) in &cameras_3d {
        if camera.is_active {
            commands.get_or_spawn(entity).insert((
                RenderPhase::<Opaque<Position>>::default(),
            ));
        }
    }
}
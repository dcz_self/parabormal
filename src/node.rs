/* SPDX-License-Identifier: MIT or Apache-2.0
Based on Bevy
commit ea4aeff9ec8e204b8d4bc8cbb10c28e90fe271ff
file crates/bevy_core_pipeline/src/core_3d/main_pass_3d_node.rs
*/
/*! A render node doing only one opaque pass */

use bevy::core_pipeline::{
    clear_color::{ClearColor, ClearColorConfig},
    core_3d::{Camera3d, Opaque3d},
};
use bevy::ecs::prelude::*;
use bevy::render::{
    camera::ExtractedCamera,
    render_graph::{Node, NodeRunError, RenderGraphContext, SlotInfo, SlotType},
    render_phase::{CachedRenderPipelinePhaseItem, DrawFunctionId, DrawFunctions, EntityPhaseItem, PhaseItem, RenderPhase, TrackedRenderPass},
    render_resource::{CachedRenderPipelineId, LoadOp, Operations, RenderPassColorAttachment, RenderPassDescriptor, RenderPassDepthStencilAttachment},
    renderer::RenderContext,
    view::{ExtractedView, ViewTarget},
};
use std::marker::PhantomData;

use wgpu;

// Why is this here? Did the material plugin take over Opaque3d and do wrong things?
// We turn this into a generic over a unit newtype to make sure different pipelines don't get in the way of each other.
pub struct Opaque<T: Send + Sync + 'static> {
    opaque: Opaque3d,
    kind: PhantomData<T>,
}

impl<T: Send + Sync + 'static> Opaque<T> {
    pub fn new(opaque: Opaque3d) -> Self {
        Self {
            opaque,
            kind: Default::default(),
        }
    }
}

impl<T: Send + Sync + 'static> PhaseItem for Opaque<T> {
    type SortKey = <Opaque3d as PhaseItem>::SortKey;

    #[inline]
    fn sort_key(&self) -> Self::SortKey {
        self.opaque.sort_key()
    }

    #[inline]
    fn draw_function(&self) -> DrawFunctionId {
        self.opaque.draw_function()
    }

    #[inline]
    fn sort(items: &mut [Self]) {
        radsort::sort_by_key(items, |item| -item.opaque.distance);
    }
}

impl<T: Send + Sync + 'static> EntityPhaseItem for Opaque<T> {
    #[inline]
    fn entity(&self) -> Entity {
        self.opaque.entity()
    }
}

impl<T: Send + Sync + 'static> CachedRenderPipelinePhaseItem for Opaque<T> {
    #[inline]
    fn cached_pipeline(&self) -> CachedRenderPipelineId {
        self.opaque.cached_pipeline()
    }
}

pub trait RenderView {
    fn get_color_attachment(&self, op: Operations<wgpu::Color>) -> RenderPassColorAttachment;
}

impl RenderView for ViewTarget {
    fn get_color_attachment(&self, op: Operations<wgpu::Color>) -> RenderPassColorAttachment {
        ViewTarget::get_color_attachment(self, op)
    }
}

// T is the target color texture component
pub struct MainPass3dNode<T, P>
    where 
    T: Component + RenderView,
    P: PhaseItem + EntityPhaseItem + CachedRenderPipelinePhaseItem,
{
    query: QueryState<
        (
            &'static ExtractedCamera,
            &'static RenderPhase<P>,
            &'static Camera3d,
            &'static T,
            &'static ViewDepthTexture,
        ),
        With<ExtractedView>,
    >,
}

impl<T, P> MainPass3dNode<T, P>
    where 
    T: Component + RenderView,
    P: PhaseItem + EntityPhaseItem + CachedRenderPipelinePhaseItem,
{
    pub const IN_VIEW: &'static str = "view";

    pub fn new(world: &mut World) -> Self {
        Self {
            query: world.query_filtered(),
        }
    }
}

impl<T, P> Node for MainPass3dNode<T, P>
    where 
    T: Component + RenderView,
    P: PhaseItem + EntityPhaseItem + CachedRenderPipelinePhaseItem,
{
    fn input(&self) -> Vec<SlotInfo> {
        vec![SlotInfo::new(MainPass3dNode::<T, P>::IN_VIEW, SlotType::Entity)]
    }

    fn update(&mut self, world: &mut World) {
        self.query.update_archetypes(world);
    }

    fn run(
        &self,
        graph: &mut RenderGraphContext,
        render_context: &mut RenderContext,
        world: &World,
    ) -> Result<(), NodeRunError> {
        let view_entity = graph.get_input_entity(Self::IN_VIEW)?;
        let (camera, opaque_phase, camera_3d, target, depth) =
            match self.query.get_manual(world, view_entity) {
                Ok(query) => query,
                Err(_) => {
                    return Ok(());
                } // No window
            };

        // Always run opaque pass to ensure screen is cleared
        {
            // Run the opaque pass, sorted front-to-back
            // NOTE: Scoped to drop the mutable borrow of render_context
            let pass_descriptor = RenderPassDescriptor {
                label: Some("main_position_pass_3d"),
                // NOTE: The opaque pass loads the color
                // buffer as well as writing to it.
                color_attachments: &[Some(target.get_color_attachment(Operations {
                    load: match camera_3d.clear_color {
                        ClearColorConfig::Default => {
                            LoadOp::Clear(world.resource::<ClearColor>().0.into())
                        }
                        ClearColorConfig::Custom(color) => LoadOp::Clear(color.into()),
                        ClearColorConfig::None => LoadOp::Load,
                    },
                    store: true,
                }))],
                depth_stencil_attachment: Some(RenderPassDepthStencilAttachment {
                    view: &depth.0.view,
                    // NOTE: 0.0 is the far plane due to bevy's use of reverse-z projections.
                    depth_ops: Some(Operations {
                        load: camera_3d.depth_load_op.clone().into(),
                        store: true,
                    }),
                    stencil_ops: None,
                }),
            };

            let draw_functions = world.resource::<DrawFunctions<P>>();

            let render_pass = render_context
                .command_encoder
                .begin_render_pass(&pass_descriptor);
            let mut draw_functions = draw_functions.write();
            let mut tracked_pass = TrackedRenderPass::new(render_pass);
            if let Some(viewport) = camera.viewport.as_ref() {
                tracked_pass.set_camera_viewport(viewport);
            }
            for item in &opaque_phase.items {
                let draw_function = draw_functions.get_mut(item.draw_function()).unwrap();
                draw_function.draw(world, &mut tracked_pass, view_entity, item);
            }
        }

        Ok(())
    }
}

use bevy::render::render_resource::{Extent3d, TextureDescriptor, TextureDimension, TextureFormat, TextureUsages};
use bevy::render::renderer::RenderDevice;
use bevy::render::texture::TextureCache;
use bevy::utils::HashMap;

#[derive(Component)]
pub struct ViewDepthTexture(pub bevy::render::view::ViewDepthTexture);

// TODO: move to aim
pub fn prepare_core_3d_depth_textures<P: PhaseItem + EntityPhaseItem + CachedRenderPipelinePhaseItem>(
    mut commands: Commands,
    mut texture_cache: ResMut<TextureCache>,
    render_device: Res<RenderDevice>,
    views_3d: Query<
        (Entity, &ExtractedCamera),
        With<RenderPhase<P>>,
    >,
) {
    let mut textures = HashMap::default();
    for (entity, camera) in &views_3d {
        if let Some(physical_target_size) = camera.physical_target_size {
            let cached_texture = textures
                .entry(camera.target.clone())
                .or_insert_with(|| {
                    texture_cache.get(
                        &render_device,
                        TextureDescriptor {
                            label: Some("view_custom_depth_texture"),
                            size: Extent3d {
                                depth_or_array_layers: 1,
                                width: physical_target_size.x,
                                height: physical_target_size.y,
                            },
                            mip_level_count: 1,
                            sample_count: 1,
                            dimension: TextureDimension::D2,
                            format: TextureFormat::Depth32Float, /* PERF: vulkan docs recommend using 24
                                                                  * bit depth for better performance */
                            usage: TextureUsages::RENDER_ATTACHMENT | TextureUsages::TEXTURE_BINDING,
                        },
                    )
                })
                .clone();
            commands.entity(entity).insert(
                ViewDepthTexture(bevy::render::view::ViewDepthTexture {
                    texture: cached_texture.texture,
                    view: cached_texture.default_view,
                })
            );
        }
    }
}
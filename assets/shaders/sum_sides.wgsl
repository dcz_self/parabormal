// const width: u32 = 128;
//var height: u32 = 128;

@group(0) @binding(0)
var texture: texture_2d<f32>;
@group(0) @binding(1)
var<storage,read_write> verticals: array<f32>;
@group(0) @binding(2)
var<storage,read_write> horizontals: array<f32>;


@compute @workgroup_size(128, 1, 1)
fn compute(@builtin(global_invocation_id) invocation_id: vec3<u32>) {
    let index = invocation_id.x;
    
    let row_idx = i32(index);
    var row_sum: f32 = 0.0;
    for (var x: i32 = 0; x < i32(128); x++) {
        let pixel = textureLoad(texture, vec2<i32>(x, row_idx), 0);
        row_sum += pixel.r;
    }
    horizontals[u32(row_idx)] = row_sum;
    
    let col_idx = i32(index);
    var col_sum: f32 = 0.0;
    for (var y: i32 = 0; y < i32(128); y++) {
        let pixel = textureLoad(texture, vec2<i32>(col_idx, y), 0);
        col_sum += pixel.r;
    }
    verticals[u32(col_idx)] = col_sum;
}
#import bevy_pbr::mesh_view_bindings
#import bevy_pbr::mesh_bindings

@group(1) @binding(0)
var<uniform> warp_enable: u32;
@group(1) @binding(1)
var<uniform> shot_origin: vec3<f32>;
@group(1) @binding(2)
var<uniform> is_target: u32;

// NOTE: Bindings must come before functions that use them!
#import bevy_pbr::mesh_functions

struct Vertex {
    @location(0) position: vec3<f32>,
};

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
};

fn warp(p: vec3<f32>) -> vec3<f32> {
    let velocity = 1.0;
    let gravity = 1.0;
    
    let local = p - shot_origin;
    let x = local.x;
    let y = local.y;

    // from https://en.wikipedia.org/wiki/Projectile_motion#Angle_%CE%B8_required_to_hit_coordinate_(x,_y)
    let v2 = velocity * velocity;
    let s = sqrt(v2 * v2 - gravity * (gravity * x * x + 2.0 * y * v2));
    let lower_angles_tan = (v2 - s) / (gravity * x);

    let cosangle = 1.0 / sqrt(lower_angles_tan * lower_angles_tan + 1.0);
    let time_of_flight = x / (velocity * cosangle);
    let direction = normalize(local.xz) * time_of_flight;
    let distance = dot(direction, direction);
    let height = lower_angles_tan * distance;
    
    let xz = shot_origin.xz + direction;
    return vec3(xz.x, shot_origin.y + height, xz.y);
}

@vertex
fn vertex(vertex: Vertex) -> VertexOutput {
    var out: VertexOutput;
    var warpos: vec4<f32>;
    warpos = mesh_position_local_to_clip(mesh.model, vec4<f32>(vertex.position, 1.0));
    //warpos.y += distance(warpos.xz, shot_origin.xz) / 5.0;
    warpos = vec4(warp(warpos.xyz), warpos.w);
    out.clip_position = warpos;
    return out;
}

@fragment
fn fragment(vo: VertexOutput) -> @location(0) vec4<f32> {
    if is_target == u32(0) {
        return vec4<f32>(0.0, 0.0, 0.0, 0.0);
    } else {
        var center: vec2<f32>;
        center = vec2<f32>(view.viewport[2], view.viewport[3]) / 2.0;
        var distance_2: vec2<f32>;
        distance_2 = (center - vo.clip_position.xy) / 100.0;
        var density: f32 = exp(- distance_2.x * distance_2.x - distance_2.y * distance_2.y);
        return vec4<f32>(1.0, density, 0.0, 0.0);
    }
}

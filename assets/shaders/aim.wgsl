#import bevy_pbr::mesh_types
#import bevy_pbr::mesh_view_bindings

@group(1) @binding(0)
var<uniform> mesh: Mesh;

@group(2) @binding(0)
var<uniform> mesh_flags: u32;

// NOTE: Bindings must come before functions that use them!
#import bevy_pbr::mesh_functions

struct Vertex {
    @location(0) position: vec3<f32>,
};

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    #import bevy_pbr::mesh_vertex_output
};

fn world_warp(world_position: vec3<f32>) -> vec3<f32> {
    let velocity = 10.0;
    let gravity = 10.0;//19.5;
    let shot_origin = view.world_position;

    let local = world_position - shot_origin;
    let x = local.x;
    let y = local.y;
    
    // from https://en.wikipedia.org/wiki/Projectile_motion#Angle_%CE%B8_required_to_hit_coordinate_(x,_y)
    let v2 = velocity * velocity;
    let s = sqrt(v2 * v2 - gravity * (gravity * x * x + 2.0 * y * v2));
    let lower_angles_tan = (v2 - s) / (gravity * x);

    let cosangle = 1.0 / sqrt(lower_angles_tan * lower_angles_tan + 1.0);
    let time_of_flight = x / (velocity * cosangle);
    let direction = normalize(local.xz) * time_of_flight;
    let distance = dot(direction, direction);
    let height_diff = lower_angles_tan * distance;

    var out = world_position;
    out.y = world_position.y + height_diff;
    return out;
}

@vertex
fn vertex(vertex: Vertex) -> VertexOutput {
    let model = mesh.model;

    var out: VertexOutput;
    out.world_position = mesh_position_local_to_world(model, vec4<f32>(vertex.position, 1.0));
    //out.world_position.y += distance(out.world_position.xz, view.world_position.xz) / 5.0;
    //out.world_position = vec4(world_warp(out.world_position.xyz), out.world_position.w);
    out.clip_position = mesh_position_world_to_clip(out.world_position);
    return out;
}

@fragment
fn fragment(vertex: VertexOutput) -> @location(0) vec4<f32> {
    if mesh_flags == u32(0) {
        return vec4<f32>(0.0, 0.0, 1.0, 1.0);
    } else {
        var center: vec2<f32>;
        center = vec2<f32>(view.viewport[2], view.viewport[3]) / 2.0;
        var distance_2 = (center - vertex.clip_position.xy) / 100.0;
        var density: f32 = exp(- distance_2.x * distance_2.x - distance_2.y * distance_2.y);
        return vec4<f32>(1.0, density, 0.0, 0.0);
    }
}

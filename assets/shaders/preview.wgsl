#import bevy_pbr::mesh_types
#import bevy_pbr::mesh_view_bindings

@group(1) @binding(0)
var<uniform> mesh: Mesh;

struct Projectile {
    projection: mat4x4<f32>,
    velocity: f32,
    gravity: f32,
}

@group(2) @binding(0)
var<uniform> projectile: Projectile;
@group(3) @binding(0)
var projectile_texture: texture_depth_2d;
@group(3) @binding(1)
var projectile_texture_sampler: sampler_comparison;

// NOTE: Bindings must come before functions that use them!
#import bevy_pbr::mesh_functions

struct Vertex {
    @location(0) position: vec3<f32>,
};

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) world_position: vec4<f32>,
    @location(1) projectiled_position: vec4<f32>,
};

fn world_warp(world_position: vec3<f32>) -> vec3<f32> {
    let velocity = projectile.velocity;
    let gravity = projectile.gravity;
    let shot_origin = view.world_position;

    let local = world_position - shot_origin;
    let x = local.x;
    let y = local.y;
    
    // from https://en.wikipedia.org/wiki/Projectile_motion#Angle_%CE%B8_required_to_hit_coordinate_(x,_y)
    let v2 = velocity * velocity;
    let s = sqrt(v2 * v2 - gravity * (gravity * x * x + 2.0 * y * v2));
    let lower_angles_tan = (v2 - s) / (gravity * x);

    let cosangle = 1.0 / sqrt(lower_angles_tan * lower_angles_tan + 1.0);
    let time_of_flight = x / (velocity * cosangle);
    let direction = normalize(local.xz) * time_of_flight;
    let distance = dot(direction, direction);
    let height_diff = lower_angles_tan * distance;

    var out = world_position;
    out.y = world_position.y + height_diff;
    return out;
}

@vertex
fn vertex(vertex: Vertex) -> VertexOutput {
    let model = mesh.model;

    var out: VertexOutput;
    out.world_position = mesh_position_local_to_world(model, vec4<f32>(vertex.position, 1.0));
    out.clip_position = mesh_position_world_to_clip(out.world_position);
    //out.projectiled_position = out.clip_position;
    out.projectiled_position = projectile.projection * out.world_position;
    // TODO: warp nonlinearly
    //out.projectiled_position = vec4(world_warp(out.projectiled_position.xyz), out.projectiled_position.w);

    return out;
}

/// FIXME: make Gaussian distribution summing to 1
fn get_density(w: f32, h: f32, position: vec2<f32>) -> f32 {
    var center = vec2<f32>(w, h) / 2.0;
    var distance_2 = (center - position) / 100.0;
    return exp(- distance_2.x * distance_2.x - distance_2.y * distance_2.y);
}

@fragment
fn fragment(vertex: VertexOutput) -> @location(0) vec4<f32> {
    let projectiled_position = vertex.projectiled_position.xyz / vertex.projectiled_position.w;
    let position = (projectiled_position.xy + vec2<f32>(1.0, 1.0)) / 2.0;
    // 1.0 => sample farther away than reference (reference reached: light)
    let level = textureSampleCompare(
        projectile_texture,
        projectile_texture_sampler,
        vec2<f32>(position.x, -position.y + 1.0),
        // Here, the actual values differ by about 0.00007 (depth texture=higher). no idea why.
        // But bias can help.
        projectiled_position.z + 0.001,
        vec2<i32>(0, 0),
    );

    var density = 0.0;
    if level > 0.0 {
        let position = (projectiled_position.xy + vec2<f32>(1.0, 1.0)) / 2.0;
        density = get_density(400.0, 400.0, position * 400.0);
    }
    // FIXME: actually calculate percentage thresholds
    let chance = round(density * 3.0);
    
    return vec4<f32>(
        chance - 1.0,
        (-abs(chance - 1.5) + 1.0) * 2.0,
        0.0,
        //position.x,
        //position.y * 2.0 - 1.0,
        //1.0,
        //density,
        //chance,
        //(projectiled_position.z - 0.01) * 100.0,
        //projd,
        //level,
        1.0,
    );
}

Parabormal
=======

An experiment in rendering.

The main experiment is in aim_distr.rs and very unfinished.

Minimal render graph example
---------------------------------------------------------------------

This shows how to create a custom render graph, and a custom node which renders meshes.

```
cargo run --bin locate3d
```

In this case, the custom node doesn't use any materials, and instead only shows some geometric properties.

Most of the files in the src directory are lightly modified files from Bevy, and they only exist to help this example get off the ground.

Quality
---------

Very works-for-me.

License
---------

Same as bevy.